ARG CI_REGISTRY
FROM ${CI_REGISTRY}/sw4j-net/jre8:latest

RUN <<EOF
apt-get update
apt-get -y upgrade
apt-get -y install temurin-8-jdk
EOF
